import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private endpoint;

  constructor(private http: HttpClient) {
    this.endpoint = environment.endpoint;
  }

  getNews(){
    return this.http.get(this.endpoint + '/servicios/noticias');
  }

  getPrograms(){
    return this.http.get(this.endpoint + '/servicios/programas');
  }

  createRecord(data){
    return this.http.post(this.endpoint + '/servicios/registro', data);
  }
}
