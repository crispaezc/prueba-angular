import {Component, Inject, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DialogComponent} from '../../dialog/dialog.component';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css']
})
export class RegistroComponent implements OnInit {

  public programs: any = [];

  public data: any = {
    name: '',
    program: null,
    comment: ''
  };

  constructor(public dialog: MatDialog, private apiService: ApiService) {
  }

  ngOnInit(): void {
    this.loadPrograms();
  }

  loadPrograms(): void {
    this.apiService.getPrograms()
      .subscribe(data => {
        this.programs = data;
      });
  }

  newRecord() {
    if (this.data.name === '' ||
      this.data.program === null ||
      this.data.program === '' ||
      this.data.comment === '') {
      this.openDialog('Error', 'Por favor ingrese todos los datos.');
      return false;
    }

    this.apiService.createRecord({})
      .subscribe(result => {
        console.log(result);
        this.openDialog('Registro exitoso', 'Se han guardado los datos correctamente.');
      });
  }
  openDialog(title, message) {
    this.dialog.open(DialogComponent, {
      data: {
        title,
        message
      }
    });
  }

}
