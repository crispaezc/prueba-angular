import {Component, OnInit} from '@angular/core';
import {ApiService} from '../../services/api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  public dataNews: any = [];

  constructor(private apiService: ApiService) {
  }

  ngOnInit(): void {
    this.getNews();
  }

  getNews(): void {
    this.apiService.getNews()
      .subscribe(data => {
        console.log(data);
        this.dataNews = data;
      });
  }

}
